const draggables = document.querySelectorAll('.taskItem')
const containers = document.querySelectorAll('.containerTask')

draggables.forEach(draggable => {
  draggable.addEventListener('dragstart', () => {
    draggable.classList.add('selected')
  })

  draggable.addEventListener('dragend', () => {
    draggable.classList.remove('selected')
  })
})

containers.forEach(container => {
  container.addEventListener('dragover', e => {
    e.preventDefault()
    const afterElement = getDragAfterElement(container, e.clientY)
    const draggable = document.querySelector('.selected')
    if (afterElement == null) {
      container.appendChild(draggable)
    } else {
      container.insertBefore(draggable, afterElement)
    }
  })
})

function getDragAfterElement(container, y) {
  const draggableElements = [...container.querySelectorAll('.draggable:not(.selected)')]
  printNewInfo()

  return draggableElements.reduce((closest, child) => {
    const box = child.getBoundingClientRect()
    const offset = y - box.top - box.height / 2
    if (offset < 0 && offset > closest.offset) {
      return { offset: offset, element: child }
    } else {
      return closest
    }
  }, { offset: Number.NEGATIVE_INFINITY }).element
}


// processing static information
let amountTasks = document.querySelectorAll(".amountTasks");
let activeTasks = document.querySelector(".active_tasks");
let completedTasks = document.querySelector(".completed_tasks");

// меняет информацию в заголовке
function printNewInfo(){
    printRightTime()
    amountTasks.forEach((task, index) => {
        index == 0 ? 
            task.innerText  = "("+ activeTasks.childElementCount +")" :
            task.innerText  = "("+ completedTasks.childElementCount +")"
    })
}    

// правильно отображение статуса выполнения 
function printRightTime(){
    let activeTasksArray = Array.from(activeTasks.children);
    activeTasksArray.forEach(task =>{
        task.children[1].children[1].children[2].style.display = "none"
        task.children[1].children[1].children[1].style.display = "block"
    })

    let CompleteTasksArray = Array.from(completedTasks.children);
    CompleteTasksArray.forEach(task =>{
        task.children[1].children[1].children[1].style.display = "none"
        task.children[1].children[1].children[2].style.display = "block"
    })
}    
printRightTime()

