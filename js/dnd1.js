const tasksListActive = document.querySelector(".active_tasks");
const taskElemnts = document.querySelectorAll(".taskItem");
const containersTask = document.querySelectorAll(".containerTask");

tasksListActive.addEventListener("dragstart", (e)=>{
    e.target.classList.add("selected");
});
tasksListActive.addEventListener("dragend", (e)=>{
    e.target.classList.remove("selected");
});

tasksListActive.addEventListener("dragover", (e)=>{
    e.preventDefault();
    
    // элемент по которму нажали
    const activeElement = tasksListActive.querySelector(`.selected`);

    const currentElement = e.target;

    const isMoveable = tasksListActive !== currentElement &&
        currentElement.classList.contains(`taskItem`);

    if (!isMoveable) {
        return;
      }
    // Находим элемент, перед которым будем вставлять
    const nextElement = (currentElement === activeElement.nextElementSibling) ?
        currentElement.nextElementSibling :
        currentElement;
    
    // Вставляем activeElement перед nextElement
    tasksListActive.insertBefore(activeElement, nextElement); 

    // console.log(tasksListActive.querySelector('.selected'));

    // console.log("currentElement")
    // console.log(currentElement)
});


const getNextElement = (cursorPosition, currentElement) => {
    // Получаем объект с размерами и координатами
    const currentElementCoord = currentElement.getBoundingClientRect();
    // Находим вертикальную координату центра текущего элемента
    const currentElementCenter = currentElementCoord.y + currentElementCoord.height / 2;
  
    // Если курсор выше центра элемента, возвращаем текущий элемент
    // В ином случае — следующий DOM-элемент
    const nextElement = (cursorPosition < currentElementCenter) ?
        currentElement :
        currentElement.nextElementSibling;
  
    return nextElement;
};
