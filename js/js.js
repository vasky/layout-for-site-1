let  firstPlay = 0

function responsiveMenu(){

    const navbar = document.getElementsByClassName("adaptiveMenu");
    const menu = document.getElementsByClassName("navbar__menu");

    if(firstPlay){
        if(navbar.className === "adaptiveMenu"){
            navbar.className += " responsive";
            menu[0].style.display = "block"
        }else{
            navbar.className = "adaptiveMenu";
            menu[0].style.display = "none"
        }
    }else{
        firstPlay = 1;
        menu[0].style.display = "block"
    }    

}    
// get navbar menu

